/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author vrglh
 * Classe Singleton
 */
class BancoDeDados {
    //Singleton; Thread-safe (porque atributos estáticos criados quando declarados
    //são garantidos de serem criados na primeira vez que são acessados)
    public final static BancoDeDados INSTANCE = new BancoDeDados();
    private final Map<Integer, Candidato> ListaCandidatos = new ConcurrentHashMap<>();
    
    private BancoDeDados() { //Private para previnir inicialização
        loadCsvBase(Constants.CSVBASE);
    }

    protected Collection<Candidato> getListaCandidatos() { //Deep copy
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(ListaCandidatos.values());
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            return (Collection<Candidato>) ois.readObject();
       }
       catch (Exception e) {
            e.printStackTrace();
            return null;
       }
    }
    
    protected void updateVotos(Collection<Candidato> Candidatos) {
        Candidatos.stream().forEach((candidato) -> {
            ListaCandidatos.get(candidato.getCodigo_votacao()).incrNum_votos(candidato.getNum_votos());
        });
        
        FileWriter writer = null;
        String line = "";
        
        try {
	    writer = new FileWriter(Constants.CSVBASE);
            
            for (Candidato candidato : ListaCandidatos.values()) {
                line = String.format("%d,%s,%s,%d\n", candidato.getCodigo_votacao(),
                        candidato.getNome_candidato(), candidato.getPartido(), candidato.getNum_votos());
                writer.append(line);
            }
	    writer.flush();
	    writer.close();
	}
	catch(IOException e) {
	     e.printStackTrace();
	}
    }
    
    //Para a serialização
    private Object readResolve() {
            return INSTANCE;
    }

    private void loadCsvBase(String csvbase) {
        String line = "";
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(csvbase));
            while ((line = reader.readLine()) != null) {
                    String[] fields = line.split(",");
                    //for (String field : fields) {
                    //    System.out.println(field);
                    //}
                    Candidato candidato = new Candidato();
                    candidato.setCodigo_votacao(Integer.parseInt(fields[0]));
                    candidato.setNome_candidato(fields[1]);
                    candidato.setPartido(fields[2]);
                    candidato.setNum_votos(Integer.parseInt(fields[3]));
                    ListaCandidatos.put(candidato.getCodigo_votacao(), candidato);
            }

	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
	}
    }
}
