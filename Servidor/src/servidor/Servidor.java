package servidor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vrglh
 */
public class Servidor {
    
    /**
    * @param args the command line arguments
    */
    public static void main(String[] args) throws InterruptedException {
        //Starts Connection Handler
        new Thread(new ConnectionHandler()).start();
        
        BancoDeDados bd = BancoDeDados.INSTANCE;
        
        while (true) {
            bd.getListaCandidatos().forEach((candidato) -> {
                System.out.println("("+candidato.getCodigo_votacao() +") "
                        +candidato.getNome_candidato()+": "+candidato.getNum_votos());
            });
            System.out.println("");
            Thread.sleep(1000);
        }
    }
    
}
