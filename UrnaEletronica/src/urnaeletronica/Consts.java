/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package urnaeletronica;

/**
 *
 * @author gutoll
 */
public final class Consts {
    public final static int PORT = 40106;
    public final static int OPCODE_LISTA = 999;
    public final static int OPCODE_VOTOS = 888;
    public final static int VOTO_BRANCO = 100000;
    public final static String ENDERECO = "cosmos.lasdpc.icmc.usp.br";
    //public final static String ENDERECO = "127.0.0.1";
    
    private Consts() {
        //Previne inicialização
    }
}
